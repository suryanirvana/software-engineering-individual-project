## Welcome to DoIt!
DoIt is a to-do list application that helps you keep track of all your activities. From adding your activities, see all of your activities, edit an activity, delete an activity, up to gaining milestone points for finishing an activity.

### How To Use DoIt
1. Run ```git clone https://gitlab.com/suryanirvana/software-engineering-individual-project.git``` in your cmd/Terminal
2. In your cmd/Terminal, redirect to the **Main** folder
3. Run ```java -cp DoIt.jar Main```
4. Application successfully started

### Documentation
- [Work log](bit.ly/SoftwareEngineeringFinalProjectWorkLog)
- [Work document](bit.ly/SoftwareEngineeringFinalProject)

### Credit
This project was created to fulfill one of the requirements in *Software Engineering* courses

Created by: I Made Gede Surya Nirvana Prima Murthi - 1806173550