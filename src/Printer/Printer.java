package Printer;

import Activity.Activity;

import java.util.List;

public class Printer {
    public void commandPrinter(String[] commands) {
        for (String commandArray : commands) {
            System.out.println("- " + commandArray);
        }
    }
    public void printAllActivities(List<Activity> listOfActivities) {
        System.out.println("You have " + listOfActivities.size() + " activity/ies");
        System.out.println("Here's your list of activities");
        for (Activity activity : listOfActivities) {
            System.out.println("-----------------------");
            System.out.println("Title: " + activity.getTitle());

            String year = String.valueOf(activity.getDueDateTime().getYear());
            String month = String.valueOf(activity.getDueDateTime().getMonth());
            String dayOfMonth = String.valueOf(activity.getDueDateTime().getDayOfMonth());
            String dueDate = dayOfMonth + "-" + month + "-" + year;
            System.out.println("Due Date: " + dueDate);

            String hours = String.valueOf(activity.getDueDateTime().getHour());
            String minutes = String.valueOf(activity.getDueDateTime().getMinute());
            String hoursInAMPM = hours;
            String AMPM = "AM";
            if (activity.getDueDateTime().getHour() > 12) {
                hoursInAMPM = String.valueOf(activity.getDueDateTime().getHour()-12);
                AMPM = "PM";
            }
            if(Integer.parseInt(hoursInAMPM) < 10) {
                hoursInAMPM = "0" + hoursInAMPM;
            }
            if (activity.getDueDateTime().getMinute() < 10) {
                minutes = "0" + minutes;
            }
            String dueTime = hours + "." + minutes + " (" + hoursInAMPM + "." + minutes + " " + AMPM + ")";
            System.out.println("Due Time: " + dueTime);
            System.out.println("Description: " + activity.getDescription());
            System.out.println("Status: " + activity.getStatus());
        }
    }

    public void printActivitiesWithNumbers(List<Activity> activities) {
        long counter = 1;
        for (Activity activity : activities) {
            System.out.println(counter++ + ". " + activity.getTitle());
        }
    }
}