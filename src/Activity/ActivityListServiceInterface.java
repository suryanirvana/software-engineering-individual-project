package Activity;

import java.util.List;

public interface ActivityListServiceInterface {
    void addNewActivity(String title, String dueDate, String dueTime, String description);

    List<Activity> seeListOfActivities();

    void clearListOfActivities();

    void changeStatus(String title);

    void editActivity(String title, String dueDate, String dueTime);

    void deleteActivity(String title);

    void addMilestone();
}
