package Activity;

import java.time.LocalDateTime;

public class Activity {
    private String title;
    private LocalDateTime dueDateTime;
    private String description;
    private String status;

    public Activity(String title, LocalDateTime dueDateTime, String description) {
        this.title = title;
        this.dueDateTime = dueDateTime;
        this.description = description;
        this.status = "ON PROGRESS";
    }

    public String getTitle() {
        return title;
    }

    public LocalDateTime getDueDateTime() {
        return dueDateTime;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() { return status; }

    public void setTitle(String title) { this.title = title; }

    public void setDueDateTime(LocalDateTime dueDateTime) { this.dueDateTime = dueDateTime; }

    public void setDescription(String description) { this.description = description; }

    public void setStatus(String status) { this.status = status; }
}