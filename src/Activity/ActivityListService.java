package Activity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ActivityListService implements ActivityListServiceInterface {
    private ActivityList activities = new ActivityList();
    private Map<String, Activity> activityList = activities.getActivityList();


    @Override
    public void addNewActivity(String title, String dueDate, String dueTime, String description) {
        String[] dueDateArray = dueDate.split("-");
        String[] dueTimeArray = dueTime.split("\\.");

        int year = Integer.parseInt(dueDateArray[2]);
        int month = Integer.parseInt(dueDateArray[1]);
        int dayOfMonth = Integer.parseInt(dueDateArray[0]);
        int hours = Integer.parseInt(dueTimeArray[0]);
        int minutes = Integer.parseInt(dueTimeArray[1]);
        LocalDateTime dueDateTime = LocalDateTime.of(year, month, dayOfMonth, hours, minutes);

        Activity newActivity = new Activity(title, dueDateTime, description);

        activityList.put(title, newActivity);

        System.out.println("Successfully added to list");
    }

    @Override
    public List<Activity> seeListOfActivities() {
        List<Activity> listOfActivities = new ArrayList<>();
        for (Activity activity : activityList.values()) {
            listOfActivities.add(activity);
        }
        return listOfActivities;
    }

    @Override
    public void clearListOfActivities() {
        activityList.clear();
        System.out.println("Successfully cleared your list of activities");
    }

    @Override
    public void changeStatus(String title) {
        Activity activity = activityList.get(title);
        activity.setStatus("FINISHED");
        System.out.println("Successfully change the status");
        System.out.println("Congratulations you have finished an activity!");
    }

    @Override
    public void editActivity(String title, String dueDate, String dueTime) {
        LocalDateTime dueDateTime = activityList.get(title).getDueDateTime();
        String[] dueDateArray = new String[1];
        String[] dueTimeArray = new String[1];
        int year = 0;
        int month = 0;
        int dayOfMonth = 0;
        int hours = 0;
        int minutes = 0;

        if (!dueDate.equals("")) {
            dueDateArray = dueDate.split("-");

            year = Integer.parseInt(dueDateArray[2]);
            month = Integer.parseInt(dueDateArray[1]);
            dayOfMonth = Integer.parseInt(dueDateArray[0]);
        }

        if (!dueTime.equals("")) {
            dueTimeArray = dueTime.split("\\.");

            hours = Integer.parseInt(dueTimeArray[0]);
            minutes = Integer.parseInt(dueTimeArray[1]);
        }

        if (!dueDate.equals("") && !dueTime.equals("")) {
            dueDateTime = LocalDateTime.of(year, month, dayOfMonth, hours, minutes);
        } else if (!dueDate.equals("")){
            dueDateTime = LocalDateTime.of(year, month, dayOfMonth, dueDateTime.getHour(), dueDateTime.getMinute());
        } else if (!dueTime.equals("")) {
            dueDateTime = LocalDateTime.of(dueDateTime.getYear(), dueDateTime.getMonth(), dueDateTime.getDayOfMonth(), hours, minutes);
        }

        Activity activity = activityList.get(title);
        activity.setDueDateTime(dueDateTime);
        System.out.println("Successfully changed the due date and time");
    }

    @Override
    public void deleteActivity(String title) {
        activityList.remove(title);
        System.out.println("Successfully deleted the activity");
    }

    @Override
    public void addMilestone() {
        activities.setMilestone(activities.getMilestone()+1);
    }
}
