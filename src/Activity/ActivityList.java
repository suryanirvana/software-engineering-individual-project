package Activity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityList {
    private Map<String, Activity> activityList = new HashMap<String, Activity>();
    private long milestone = 0;

    public Map<String, Activity> getActivityList() {
        return activityList;
    }

    public long getMilestone() {
        return milestone;
    }

    public void setMilestone(long milestone) {
        this.milestone = milestone;
    }
}