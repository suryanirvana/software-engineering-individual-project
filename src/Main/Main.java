package Main;

import User.*;
import Printer.*;
import java.io.*;

public class Main {
    private static PrintWriter out;
    private static Loader loader = new Loader();
    private static Printer printer = new Printer();

    public static void main(String args[]) throws IOException {
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);
        loader.readInput(printer);
        out.close();
    }
}