package Test;

import Activity.*;
import Printer.*;

import java.io.*;
import java.util. *;

import java.io.InputStream;

public class DoItTest {
    private static InputReader in;
    private static ActivityList activityList = new ActivityList();
    private static ActivityListService activityService = new ActivityListService();
    private static Printer printer = new Printer();

    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        System.out.println("-----------------------");
        System.out.println("Welcome to DO IT!");
        System.out.println("LET'S GET THINGS DONE");
        System.out.println("-----------------------");
        System.out.println("You currently have " + activityList.getMilestone() + " milestone points");

        System.out.print("Command: ");
        String command = "ADD";
        System.out.println(command);

        String title = "Test Activity";
        String dueDateValid = "20-12-2020";
        String dueTimeValid = "20.20";
        String description = "This is a test";

        String dueDateNotValid = "20-20-2020";
        String dueTimeNotValid = "24.60";

        try {
            /*
             * Test for valid input
             */
            System.out.println("This is your valid input");
            System.out.println("Title: " + title);
            System.out.println("Due Date: " + dueDateValid);
            System.out.println("Due Time: " + dueTimeValid);
            System.out.println("Description: " + description);

            activityService.addNewActivity(title, dueDateValid, dueTimeValid, description);

            System.out.println("-----------------------");

            /*
             * Test for invalid input
             */
            System.out.println("This is your invalid input");
            System.out.println("Title: " + title);
            System.out.println("Due Date: " + dueDateNotValid);
            System.out.println("Due Time: " + dueTimeNotValid);
            System.out.println("Description: " + description);

            activityService.addNewActivity(title, dueDateNotValid, dueTimeNotValid, description);

            System.out.println("-----------------------");
        } catch (Exception e) {
            System.out.println("Invalid input, activity is not added to the list");
            System.out.println("-----------------------");
        }
        System.out.println("You currently have " + activityList.getMilestone() + " milestone points");

        System.out.print("Command: ");
        command = "SEE";
        System.out.println(command);

        List<Activity> activities = activityService.seeListOfActivities();
        printer.printAllActivities(activities);

        System.out.println("-----------------------");
        System.out.println("You currently have " + activityList.getMilestone() + " milestone points");
        System.out.print("Command: ");
        command = "EDIT";
        System.out.println(command);

        int counter = 1;
        activities = activityService.seeListOfActivities();
        for (Activity activity : activities) {
            System.out.println(counter++ + ". " + activity.getTitle());
        }
        System.out.print("Activity number: ");
        int input = 1;
        String titleActivity = activities.get(input-1).getTitle();
        System.out.println(input);

        String editedDueDate = "12-12-2020";
        String editedDueTime = "12.12";
        System.out.println("New due date: " + editedDueDate);
        System.out.println("New due time: " + editedDueTime);
        activityService.editActivity(titleActivity, editedDueDate, editedDueTime);

        System.out.println("-----------------------");
        System.out.print("Command: ");
        command = "SEE";
        System.out.println(command);

        activities = activityService.seeListOfActivities();
        printer.printAllActivities(activities);

        System.out.println("-----------------------");
        System.out.println("You currently have " + activityList.getMilestone() + " milestone points");
        System.out.print("Command: ");
        command = "EDIT";
        System.out.println(command);

        counter = 1;
        activities = activityService.seeListOfActivities();
        for (Activity activity : activities) {
            System.out.println(counter++ + ". " + activity.getTitle());
        }
        System.out.print("Activity number: ");
        input = 1;
        titleActivity = activities.get(input-1).getTitle();
        System.out.println(input);

        editedDueDate = "12-04-2020";
        editedDueTime = "";
        System.out.println("New due date: " + editedDueDate);
        System.out.println("New due time: " + editedDueTime);
        activityService.editActivity(titleActivity, editedDueDate, editedDueTime);

        System.out.println("-----------------------");
        System.out.print("Command: ");
        command = "SEE";
        System.out.println(command);

        activities = activityService.seeListOfActivities();
        printer.printAllActivities(activities);

        System.out.println("-----------------------");
        System.out.println("You currently have " + activityList.getMilestone() + " milestone points");
        System.out.print("Command: ");
        command = "MARK";
        System.out.println(command);

        counter = 1;
        activities = activityService.seeListOfActivities();
        for (Activity activity : activities) {
            System.out.println(counter++ + ". " + activity.getTitle());
        }
        System.out.print("Activity number: ");
        input = 1;
        titleActivity = activities.get(input-1).getTitle();
        System.out.println(input);
        activityService.changeStatus(titleActivity);

        System.out.println("-----------------------");
        System.out.print("Command: ");
        command = "SEE";
        System.out.println(command);

        activities = activityService.seeListOfActivities();
        printer.printAllActivities(activities);

        System.out.println("-----------------------");
        System.out.println("You currently have " + activityList.getMilestone() + " milestone points");
        System.out.print("Command: ");
        command = "DELETE";
        System.out.println(command);

        counter = 1;
        activities = activityService.seeListOfActivities();
        for (Activity activity : activities) {
            System.out.println(counter++ + ". " + activity.getTitle());
        }
        System.out.print("Activity number: ");
        input = 1;
        titleActivity = activities.get(input-1).getTitle();
        System.out.println(input);

        System.out.print("Are you sure you want to delete " + titleActivity + "?");
        System.out.print("Please choose either YES or NO: ");
        String yesOrNo = "NO";
        System.out.println(yesOrNo);
        if (yesOrNo.equals("YES")) {
            activityService.deleteActivity(titleActivity);
        } else {
            System.out.println("You decided not to delete the activity");
        }

        System.out.println("-----------------------");
        System.out.print("Command: ");
        command = "SEE";
        System.out.println(command);

        activities = activityService.seeListOfActivities();
        printer.printAllActivities(activities);

        System.out.println("-----------------------");
        System.out.println("You currently have " + activityList.getMilestone() + " milestone points");
        System.out.print("Command: ");
        command = "DELETE";
        System.out.println(command);

        counter = 1;
        activities = activityService.seeListOfActivities();
        for (Activity activity : activities) {
            System.out.println(counter++ + ". " + activity.getTitle());
        }
        System.out.print("Activity number: ");
        input = 1;
        titleActivity = activities.get(input-1).getTitle();
        System.out.println(input);

        System.out.println("Are you sure you want to delete " + titleActivity + "?");
        System.out.print("Please choose either YES or NO: ");
        yesOrNo = "YES";
        System.out.println(yesOrNo);
        if (yesOrNo.equals("YES")) {
            activityService.deleteActivity(titleActivity);
        } else {
            System.out.println("You decided not to delete the activity");
        }

        System.out.println("-----------------------");
        System.out.print("Command: ");
        command = "CLEAR";
        System.out.println(command);

        activities = activityService.seeListOfActivities();
        System.out.println("You have " + activities.size() + " activity/ies");
        activityService.clearListOfActivities();
        activities = activityService.seeListOfActivities();
        System.out.println("You now have " + activities.size() + " activity/ies");

        System.out.println("-----------------------");
        System.out.println("See you next time!");
        System.out.println("-----------------------");
    }

    // taken from https://codeforces.com/submissions/Petr
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
        public String nextLine() throws IOException{
            return reader.readLine();
        }
    }
}
