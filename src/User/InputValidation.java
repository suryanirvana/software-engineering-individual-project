package User;

import Activity.Activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.StringTokenizer;

public class InputValidation {
    private static InputStream inputStream = System.in;
    private static Loader.InputReader in = new Loader.InputReader(inputStream);

    public String titleInput() throws IOException {
        System.out.print("Title: ");
        String title = in.nextLine();
        while (title.equals("")) {
            System.out.println("Please fill in the title");
            System.out.print("Title: ");
            title = in.nextLine();
        }
        return title;
    }
    public String dueDateInput(String command) throws IOException {
        System.out.print("Due Date (DD-MM-YYYY): ");
        String dueDate = in.nextLine();
        switch (command) {
            case "EDIT":
                if (dueDate.equals("")) return "";
                break;
            default:
                while (dueDate.equals("") || dueDate.split("").length==1) {
                    System.out.println("Please fill in the due date");
                    System.out.print("Due Date (DD-MM-YYYY): ");
                    dueDate = in.nextLine();
                }
                int dayOfMonth = Integer.parseInt(dueDate.split("-")[0]);
                int month = Integer.parseInt(dueDate.split("-")[1]);
                boolean validDayOfMonth = dayOfMonth > 0 && dayOfMonth < 32;
                boolean validMonth = month > 0 && month < 13;
                while (dueDate.split("-").length!=3 || !validDayOfMonth || !validMonth) {
                    System.out.println("Please fill in the due date with the correct form (DD-MM-YYYY)");
                    System.out.print("Due Date (DD-MM-YYYY): ");
                    dueDate = in.nextLine();
                    dayOfMonth = Integer.parseInt(dueDate.split("-")[0]);
                    month = Integer.parseInt(dueDate.split("-")[1]);
                    validDayOfMonth = dayOfMonth > 0 && dayOfMonth < 32;
                    validMonth = month > 0 && month < 13;
                }
                break;
        }
        return dueDate;
    }
    public String dueTimeInput(String command) throws IOException {
        System.out.print("Due Time (HH.MM): ");
        String dueTime = in.nextLine();

        switch (command) {
            case "EDIT":
                if (dueTime.equals("")) return "";
                break;
        }

        while (dueTime.equals("") || dueTime.split("").length==1) {
            System.out.println("Please fill in the due time");
            System.out.print("Due Time (HH.MM): ");
            dueTime = in.nextLine();
        }
        int hours = Integer.parseInt(dueTime.split("\\.")[0]);
        int minutes = Integer.parseInt(dueTime.split("\\.")[1]);
        boolean validHours = hours >= 0 && hours < 24;
        boolean validMinutes = minutes >= 0 && minutes < 60;
        while (dueTime.split("\\.").length!=2 || !validHours || !validMinutes) {
            System.out.println("Please fill in the due time with the correct form (HH.MM)");
            System.out.print("Due Time (HH.MM): ");
            dueTime = in.nextLine();
            hours = Integer.parseInt(dueTime.split("\\.")[0]);
            minutes = Integer.parseInt(dueTime.split("\\.")[1]);
            validHours = hours >= 0 && hours < 24;
            validMinutes = minutes >= 0 && minutes < 60;
        }
        return dueTime;
    }
    public String descriptionInput() throws IOException {
        System.out.print("Description: ");
        String description = in.nextLine();
        if(description.equals("")) description = "-";
        return description;
    }

    public boolean activityNotFinished(List<Activity> activities, int inputMark) {
        return !activities.get(inputMark-1).getStatus().equals("FINISHED");
    }

    // taken from https://codeforces.com/submissions/Petr
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
        public String nextLine() throws IOException{
            return reader.readLine();
        }
    }
}
