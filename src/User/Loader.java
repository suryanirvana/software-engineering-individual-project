package User;

import Activity.*;
import Printer.Printer;

import java.io.*;
import java.util. *;

public class Loader {
    private static ActivityList activityList = new ActivityList();
    private static ActivityListService activityService = new ActivityListService();
    private static InputValidation inputValidation = new InputValidation();
    private static Printer printer;
    private static String[] commands = {"ADD", "SEE", "CLEAR", "MARK", "EDIT", "DELETE", "EXIT"};
    private static InputStream inputStream = System.in;
    private static InputReader in = new InputReader(inputStream);

    public static void readInput(Printer printerObj) throws IOException{
        printer = printerObj;

        System.out.println("-----------------------");
        System.out.println("Welcome to DO IT!");
        System.out.println("LET'S GET THINGS DONE");
        System.out.println("-----------------------");

        System.out.println("What do you want to do?");
        System.out.println("You currently have " + activityList.getMilestone() + " milestone points");

        printer.commandPrinter(commands);

        System.out.print("Command: ");
        String command = in.nextLine().toUpperCase();

        while (!command.equals("EXIT")) {
            List<Activity> activities = activityService.seeListOfActivities();

            switch (command) {
                case "ADD":
                    System.out.println("Fill in the form below with the activity you want to record");

                    String title = inputValidation.titleInput();
                    String dueDate = inputValidation.dueDateInput(command);
                    String dueTime = inputValidation.dueTimeInput(command);
                    String description = inputValidation.descriptionInput();

                    activityService.addNewActivity(title, dueDate, dueTime, description);
                    break;
                case "SEE":
                    printer.printAllActivities(activities);
                    break;
                case "CLEAR":
                    activityService.clearListOfActivities();
                    break;
                case "MARK":
                    if (activities.size() != 0) {
                        System.out.println("Which activity that you have finished working on?");
                        int inputMark = activityChooser(activities);
                        String markActivity = activities.get(inputMark - 1).getTitle();

                        boolean activity = inputValidation.activityNotFinished(activities, inputMark);

                        if (activity) {
                            activityService.changeStatus(markActivity);
                            System.out.println("You get 1 milestone points for finishing an activity!");
                            activityService.addMilestone();
                        } else {
                            System.out.println("You can't mark a FINISHED activity");
                        }

                    } else if (activities.size() == 0) {
                        System.out.println("You don't have any activity/ies yet");
                    }
                    break;
                case "EDIT":
                    if (activities.size() != 0) {
                        System.out.println("Which activity that you want to edit?");
                        int inputEdit = activityChooser(activities);
                        String editActivity = activities.get(inputEdit-1).getTitle();

                        boolean activity = inputValidation.activityNotFinished(activities, inputEdit);

                        if (activity) {
                            System.out.println("Please enter a new deadline for " + editActivity);
                            String editedDueDate = inputValidation.dueDateInput(command);
                            String editedDueTime = inputValidation.dueTimeInput(command);
                            activityService.editActivity(editActivity, editedDueDate, editedDueTime);
                        } else {
                            System.out.println("You can't edit a FINISHED activity");
                        }
                    } else if (activities.size() == 0) {
                        System.out.println("You don't have any activity/ies yet");
                    }
                    break;
                case "DELETE":
                    if (activities.size() != 0) {
                        System.out.println("Which activity that you want to delete?");
                        int inputDelete = activityChooser(activities);
                        String deleteActivity = activities.get(inputDelete - 1).getTitle();

                        System.out.print("Are you sure you want to delete " + deleteActivity + "?");
                        System.out.print("Please choose either YES or NO: ");
                        String yesOrNo = in.nextLine().toUpperCase();
                        if (yesOrNo.equals("YES")) {
                            activityService.deleteActivity(deleteActivity);
                        } else {
                            System.out.println("You decided not to delete the activity");
                        }
                    } else if (activities.size() == 0) {
                        System.out.println("You don't have any activity/ies yet");
                    }
                    break;
                default:
                    System.out.println("Invalid command, please enter the valid command");
            }

            System.out.println("-----------------------");
            System.out.println("What do you want to do next?");
            System.out.println("You currently have " + activityList.getMilestone() + " milestone points");

            printer.commandPrinter(commands);

            System.out.print("Command: ");
            command = in.nextLine().toUpperCase();
        }
        System.out.println("-----------------------");
        System.out.println("See you next time!");
        System.out.println("-----------------------");
    }

    public static int activityChooser(List<Activity> activities) throws IOException {
        printer.printActivitiesWithNumbers(activities);
        System.out.print("Activity number: ");
        int input = Integer.parseInt(in.nextLine());
        return input;
    }

    // taken from https://codeforces.com/submissions/Petr
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
        public String nextLine() throws IOException{
            return reader.readLine();
        }
    }
}